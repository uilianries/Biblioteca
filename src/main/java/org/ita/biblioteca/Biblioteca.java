package org.ita.biblioteca;


import java.util.HashSet;

public class Biblioteca {

    private HashSet<Usuario> _usuarios = new HashSet<Usuario>();

    public void registraUsuario(String nome)
            throws UsuarioJaRegistradoException, UsuarioComNomeVazioException,
            UsuarioInexistenteException {
        if (nome == null) {
            throw new UsuarioInexistenteException();
        }

        if (nome.isEmpty()) {
            throw new UsuarioComNomeVazioException();
        }

        Usuario usuario = new Usuario(nome);
        if (_usuarios.contains(usuario)) {
            throw new UsuarioJaRegistradoException(nome);
        }

        _usuarios.add(usuario);
    }
}
