package org.ita.biblioteca;

/**
 * Created by uilian on 9/6/16.
 */
public class Usuario {
    private String nome;

    public Usuario(String nome) {
        this.nome = nome;
    }

    @Override
    public boolean equals(Object object) {
        return object instanceof Usuario && nome.equals(((Usuario)object).nome);
    }

    @Override
    public int hashCode() {
        return this.nome.hashCode();
    }
}
