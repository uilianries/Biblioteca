package org.ita.biblioteca;

/**
 * Created by uilian on 9/6/16.
 */
public class UsuarioComNomeVazioException extends Exception {


    public UsuarioComNomeVazioException() {
        super("---> Não pode registrar usuário com nome vazio!");
    }
}
