package org.ita.biblioteca;

/**
 * Created by uilian on 9/6/16.
 */
public class UsuarioInexistenteException extends Exception {

    public UsuarioInexistenteException() {
        super("--->Não pode registrar usuário inexistente!");
    }
}
