package org.ita.biblioteca;

public class UsuarioJaRegistradoException extends Exception {

    public UsuarioJaRegistradoException(String nome) {
        super("---> Já existe usuário com nome \"" + nome + "\"! Use outro nome!");
    }
}
