package org.ita.biblioteca;

import org.junit.Before;
import org.junit.Test;


public class BibliotecaTest {
    private Biblioteca biblioteca;

    @Before
    public void createBibloteca() {
        this.biblioteca = new Biblioteca();
    }

    @Test(expected = UsuarioComNomeVazioException.class)
    public void usuarioComNomeVazio() throws UsuarioComNomeVazioException, UsuarioJaRegistradoException, UsuarioInexistenteException {
        biblioteca.registraUsuario("");
    }

    @Test(expected = UsuarioInexistenteException.class)
    public void usuarioInexistente() throws UsuarioComNomeVazioException, UsuarioJaRegistradoException, UsuarioInexistenteException {
        this.biblioteca.registraUsuario(null);
    }

    @Test(expected = UsuarioJaRegistradoException.class)
    public void usuarioJaRegistrado() throws UsuarioComNomeVazioException, UsuarioJaRegistradoException, UsuarioInexistenteException {
        final String nome = "James Gosling";
        this.biblioteca.registraUsuario(nome);
        this.biblioteca.registraUsuario(nome);
    }

    @Test
    public void registraUsuario() throws UsuarioComNomeVazioException, UsuarioJaRegistradoException, UsuarioInexistenteException {
        final String nome = "James Gosling";
        this.biblioteca.registraUsuario(nome);
    }

}